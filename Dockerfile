FROM php:7.0

# Setup env
RUN apt-get update
RUN apt-get install -y git
ADD ["id_rsa.pub", "/root/.ssh/id_rsa.pub"]
ADD ["id_rsa", "/root/.ssh/id_rsa"]
RUN chmod 400 /root/.ssh/id_rsa
RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts

#Setup project
RUN git clone git@bitbucket.org:Lucaszz/docker-testing-symfony-project.git
WORKDIR /docker-testing-symfony-project
RUN curl -sS https://getcomposer.org/installer | php
RUN php composer.phar install

CMD /bin/bash